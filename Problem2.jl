### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ f43df380-da7d-11eb-1438-79102800d1e8
using Pkg;

# ╔═╡ 4e69c167-8beb-421c-bb82-4c7a70c84ae3
Pkg.activate("Project.toml")

# ╔═╡ 4abc8f8c-7cce-424c-9cb7-1cc65b0328b2
board = [
		  [" ", " ", " "],
          [" ", " ", " "],
          [" ", " ", " "]
		]

# ╔═╡ 0429158f-772a-4dee-bd81-0fc9db5719c3
player1 = "X"

# ╔═╡ cc1a9780-a151-4ff3-9072-bcb374a950bd
player2 = "O"

# ╔═╡ a4c58bbf-cf2a-4fa8-b108-227139266984
player1_win = -1

# ╔═╡ 38b3fed7-3102-4638-88f3-f72bb8548057
tie = 0

# ╔═╡ d7e3b9f0-4e9b-4a6f-8eaa-dbe237a7dec9
player2_win = 1

# ╔═╡ 6df781e2-5acb-445f-b0af-4d1ba62c8dc9
function print_board(board)
    println("    1   2   3")
    println(" 1  ", join(board[1], " | "))
    println("   ———┼———┼———")
    println(" 2  ", join(board[2], " | "))
    println("   ———┼———┼———")
    println(" 3  ", join(board[3], " | "))
end

# ╔═╡ caa3c5cb-2abe-42c0-b1b7-95607d094481
function check_available(board)
    available_cells = []
    for i in 1:3, j in 1:3
        if board[i][j] == " "
            push!(available_cells, (i, j))
        end
    end
    return available_cells
end

# ╔═╡ 543a2024-c3d3-4901-b473-491d759351c3
function check_win(board)

    # Rows
    for i in 1:3
        if (board[i][1] == board[i][2] == board[i][3]) && (board[i][1] in [player1, player2])
            if (board[i][1] == player1)
                return player1_win
            end
            return player2_win
        end
    end

    # Column
    for i in 1:3
        if (board[1][i] == board[2][i] == board[3][i]) && (board[1][i] in [player1, player2])
            if (board[1][i] == player1)
                return player1_win
            end
            return player2_win
        end
    end

    # Principal diagonal
    if (board[1][1] == board[2][2] == board[3][3]) && (board[1][1] in [player1, player2])
        if (board[1][1] == player1)
            return player1_win
        end
        return player2_win
    end

    # Other diag
    if (board[1][3] == board[2][2] == board[3][1]) && (board[1][3] in [player1, player2])
        if (board[1][3] == player1)
            return player1_win
        end
        return player2_win
    end

    # Tie
    if length(check_available(board)) == 0
        return tie
    end

    # No one wins, game still goes on
    return nothing
end

# ╔═╡ 7b4324fe-c67c-42c1-8f99-3e4ca1c0d4b1
function make_move!(board, player, (x, y))
    board[x][y] = player
end

# ╔═╡ db5b47e4-47c1-49cf-b859-42f75291085a
function minimax(board, depth, is_maximising)
    result = check_win(board)

    if result !== nothing
        return result
    elseif is_maximising

        best_score = -Inf

        for i in 1:3, j in 1:3
            if board[i][j] == " "

                board[i][j] = player2

                score = minimax(board, depth + 1, false)
                
                board[i][j] = " "

                best_score = max(score, best_score)
            end
        end

        return best_score        
    else        
        best_score = Inf

        for i in 1:3, j in 1:3
            if board[i][j] == " "

                board[i][j] = player1

                score = minimax(board, depth + 1, true)
                
                board[i][j] = " "

                best_score = min(score, best_score)
            end
        end

        return best_score   
    end

end

# ╔═╡ 474bb31d-70cb-467c-b147-1524c0418e4b
function worst_move(board)
    worst_score = Inf

    worst_pos = (0, 0)

    for i in 1:3, j in 1:3
        if board[i][j] == " "

            board[i][j] = player2
            
            score = minimax(board, 0, true)

            
            board[i][j] = " "

            if score < worst_score
                worst_score = score
                worst_pos = (i, j)
            end
        end
    end
    return worst_pos
end

# ╔═╡ d2eb7fb9-d367-4461-bec3-ad336b4d6e60
function best_move(board)
    best_score = -Inf

    best_pos = (0, 0)

    for i in 1:3, j in 1:3
        if board[i][j] == " "

            board[i][j] = player2
            
            score = minimax(board, 0, false)

            
            board[i][j] = " "

            if score > best_score
                best_score = score
                best_pos = (i, j)
            end
        end
    end
    return best_pos
end

# ╔═╡ b0c221f0-9574-4e0a-93ba-910eeee52f62
function ai_move!(board, difficulty)
    if difficulty == "loser"
        make_move!(board, player2, worst_move(board))
    elseif difficulty == "easy"
        pos = rand(check_available(board))
        make_move!(board, player2, pos)
    elseif difficulty == "medium"
        if rand() < 0.3
            make_move!(board, player2, best_move(board))
        else            
            pos = rand(check_available(board))
            make_move!(board, player2, pos)
        end
    elseif difficulty == "hard"
        if rand() < 0.7
            make_move!(board, player2, best_move(board))
        else            
            pos = rand(check_available(board))
            make_move!(board, player2, pos)
        end
    elseif difficulty == "impossible"
        make_move!(board, player2, best_move(board))
    end
    
end

# ╔═╡ b20321db-30d9-4562-bb38-3e144c4264bf
function move_input(board, prompt, params)

    print(prompt)
    txt = split(readline())

    if txt == ["exit"]
        exit()
    end

    if length(txt) == params && txt[1] in string.(collect(1:3)) && txt[2] in string.(collect(1:3)) && (board[parse(Int, txt[1])][parse(Int, txt[2])] == " ")
        return txt
    else
        println("Please enter a valid command (See README)")
        move_input(board, prompt, params)
    end
end

# ╔═╡ adcdf5cf-51bc-4480-9074-aeec9c74d87a
function generalised_input(prompt, same_line, choices, params)
    same_line ? print(prompt) : println(prompt)

    txt = split(lowercase(readline()))

    if txt == ["exit"]
        exit()
    end

    if length(txt) == params && txt[1] in choices
        return txt
    else
        println("Please enter a valid command (See README)")
        generalised_input(prompt, same_line, choices, params)
    end
end

# ╔═╡ 4dd3db08-bc5c-41da-a749-fcb1d3fe6733
function singleplayer(board, (player1, player2), (player1_win, player2_win, tie))
    diff_levels = """
    Choose a difficulty level:
    - `loser` - AI always makes worst move
    - `easy` - AI plays randomly
    - `medium` - AI makes some mistakes
    - `hard` - AI makes very few mistakes
    - `impossible` - AI always makes best move. Impossible to win.
    """

    println(diff_levels)
    difficulty = generalised_input("Difficulty: ", true, ["loser", "easy", "medium", "hard", "impossible"], 1)[1]

    println("Human is X, AI is O. Human goes first.")
    print_board(board)

    while true

        txt = move_input(board, "Enter position of cell (row and column seperated by space): ", 2)

        make_move!(board, player1, (parse(Int, txt[1]), parse(Int, txt[2])))

        if check_win(board) == player1_win
            println("Human wins! The humans are still worthy!")
            break
        elseif check_win(board) == player2_win
            println("AI wins. Humanity has no hope.")
            break
        elseif check_win(board) == tie
            println("Tie! Have we reached the singularity?")
            break
        end

        ai_move!(board, difficulty)
        print_board(board)

        if check_win(board) == player1_win
            println("Human wins! The human are still worthy!")
            break
        elseif check_win(board) == player2_win
            println("AI wins. Humanity has no hope.")
            break
        elseif check_win(board) == tie
            println("Tie! Have we reached the singularity?")
            break
        end
    end
end

# ╔═╡ 437e0c9c-1840-438b-96f8-ed063bc3133d
function multiplayer(board, (player1, player2), (player1_win, player2_win, tie))
    println("Player 1 is X, Player 2 is O. Player 1 goes first.")
    print_board(board)

    while true

        txt = move_input(board, "Enter position of cell (row and column seperated by space): ", 2)

        make_move!(board, player1, (parse(Int, txt[1]), parse(Int, txt[2])))
        print_board(board)

        if check_win(board) == player1_win
            println("Player 1 Wins!")
            break
        elseif check_win(board) == player2_win
            println("Player 2 Wins!")
            break
        elseif check_win(board) == tie
            println("Tie!")
            break
        end

        txt = move_input(board, "Enter position of cell (row and column seperated by space): ", 2)

        make_move!(board, player2, (parse(Int, txt[1]), parse(Int, txt[2])))

        print_board(board)


        if check_win(board) == player1_win
            println("Player 1 Wins!")
            break
        elseif check_win(board) == player2_win
            println("Player 2 Wins!")
            break
        elseif check_win(board) == tie
            println("Tie!")
            break
        end
    end
end

# ╔═╡ 3313b6d2-9a93-4cd7-a902-ca451b096b7a
function play_again()
    replay = generalised_input("Play again? (y/n): ", true, ["y", "n"], 1)[1]
    if replay == "y"
        run(`julia $PROGRAM_FILE`)
        exit()
    else
        exit()
    end
end

# ╔═╡ 408fc8cf-de21-4d11-a01e-b632faa8bf8b
mode = generalised_input("Welcome to TicTacToe! Would you like to play singleplayer (1) or multiplayer (2)?", false, ["1", "2"], 1)[1]

# ╔═╡ 12a217c6-da15-4c99-8acf-de963b763f8d
if mode == "1"
    singleplayer(board, (player1, player2), (player1_win, player2_win, tie))
else
    multiplayer(board, (player1, player2), (player1_win, player2_win, tie))
end

# ╔═╡ 0c2be944-0895-45af-97c2-69d0c6dfa68f
play_again()

# ╔═╡ Cell order:
# ╠═f43df380-da7d-11eb-1438-79102800d1e8
# ╠═4e69c167-8beb-421c-bb82-4c7a70c84ae3
# ╠═4abc8f8c-7cce-424c-9cb7-1cc65b0328b2
# ╠═0429158f-772a-4dee-bd81-0fc9db5719c3
# ╠═cc1a9780-a151-4ff3-9072-bcb374a950bd
# ╠═a4c58bbf-cf2a-4fa8-b108-227139266984
# ╠═38b3fed7-3102-4638-88f3-f72bb8548057
# ╠═d7e3b9f0-4e9b-4a6f-8eaa-dbe237a7dec9
# ╠═6df781e2-5acb-445f-b0af-4d1ba62c8dc9
# ╠═caa3c5cb-2abe-42c0-b1b7-95607d094481
# ╠═543a2024-c3d3-4901-b473-491d759351c3
# ╠═7b4324fe-c67c-42c1-8f99-3e4ca1c0d4b1
# ╠═474bb31d-70cb-467c-b147-1524c0418e4b
# ╠═b0c221f0-9574-4e0a-93ba-910eeee52f62
# ╠═db5b47e4-47c1-49cf-b859-42f75291085a
# ╠═d2eb7fb9-d367-4461-bec3-ad336b4d6e60
# ╠═b20321db-30d9-4562-bb38-3e144c4264bf
# ╠═adcdf5cf-51bc-4480-9074-aeec9c74d87a
# ╠═4dd3db08-bc5c-41da-a749-fcb1d3fe6733
# ╠═437e0c9c-1840-438b-96f8-ed063bc3133d
# ╠═3313b6d2-9a93-4cd7-a902-ca451b096b7a
# ╠═408fc8cf-de21-4d11-a01e-b632faa8bf8b
# ╠═12a217c6-da15-4c99-8acf-de963b763f8d
# ╠═0c2be944-0895-45af-97c2-69d0c6dfa68f
