### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ f81d5910-d8e7-11eb-2507-793e6c246550
using Pkg;

# ╔═╡ b024fb32-5964-4604-80f5-033596304c0d
Pkg.activate("Project.toml");

# ╔═╡ c74f06cb-e2ab-40a9-a65b-b8d8f9f40359
using LinearAlgebra

# ╔═╡ 27f77087-4dc6-40f7-aebf-0781243bc225
using Distributions, Plots, Printf, QuantEcon, Random

# ╔═╡ f511d7b1-f189-4bf7-9117-bae413fdfd51
gr(fmt = :png);

# ╔═╡ 138fc6c1-a0d0-4f4d-8436-45ca7f3df323
d = Categorical([0.5, 0.3, 0.2]) # 3 discrete states

# ╔═╡ 31359b10-a8a3-48db-8fee-43e0e725e8a7
@show rand(d, 5)

# ╔═╡ bfba1531-5201-4af5-99d5-bf2043592497
@show supertype(typeof(d))

# ╔═╡ a82bf8fd-8a40-4055-a659-337ae069ae97
@show pdf(d, 1) # the probability to be in state 1

# ╔═╡ 0848160c-e4be-463d-8ee5-4a38e31d0074
@show support(d)

# ╔═╡ 83d67b87-a476-4d99-bef8-365c5819afaf
@show pdf.(d, support(d)); # broadcast the pdf over the whole support

# ╔═╡ 2a148d00-2d41-44a8-9eac-0aa23057303d
P = [0.4 0.6; 0.2 0.8]

# ╔═╡ 1c7a7acd-6b75-49d0-a43a-2a14ed3df792
mc = MarkovChain(P)

# ╔═╡ 1f7137b7-ff0f-4e80-ada6-b3d09fcbc8d7
X = simulate(mc, 100_000);

# ╔═╡ 15c5c410-15ad-43ef-8fbc-c96915f9e6e7
μ_1 = count(X .== 1)/length(X) # .== broadcasts test for equality. Could use mean(X .== 1)

# ╔═╡ 0001c7bd-1c68-4f14-8707-8405213fbc37
simulate(mc, 4, init = 1) # start at state 1

# ╔═╡ 1f0fe60b-3897-4c41-97e1-becb77085b93
simulate(mc, 4, init = 2) # start at state 2

# ╔═╡ 5a9a5c0d-73fd-489c-b04f-6e065725360a
simulate(mc, 4) # start with randomly chosen initial condition

# ╔═╡ b791989d-b0e0-4703-9032-b95b836513b9
simulate_indices(mc, 4)

# ╔═╡ Cell order:
# ╠═f81d5910-d8e7-11eb-2507-793e6c246550
# ╠═b024fb32-5964-4604-80f5-033596304c0d
# ╠═c74f06cb-e2ab-40a9-a65b-b8d8f9f40359
# ╠═27f77087-4dc6-40f7-aebf-0781243bc225
# ╠═f511d7b1-f189-4bf7-9117-bae413fdfd51
# ╠═138fc6c1-a0d0-4f4d-8436-45ca7f3df323
# ╠═31359b10-a8a3-48db-8fee-43e0e725e8a7
# ╠═bfba1531-5201-4af5-99d5-bf2043592497
# ╠═a82bf8fd-8a40-4055-a659-337ae069ae97
# ╠═0848160c-e4be-463d-8ee5-4a38e31d0074
# ╠═83d67b87-a476-4d99-bef8-365c5819afaf
# ╠═2a148d00-2d41-44a8-9eac-0aa23057303d
# ╠═1c7a7acd-6b75-49d0-a43a-2a14ed3df792
# ╠═1f7137b7-ff0f-4e80-ada6-b3d09fcbc8d7
# ╠═15c5c410-15ad-43ef-8fbc-c96915f9e6e7
# ╠═0001c7bd-1c68-4f14-8707-8405213fbc37
# ╠═1f0fe60b-3897-4c41-97e1-becb77085b93
# ╠═5a9a5c0d-73fd-489c-b04f-6e065725360a
# ╠═b791989d-b0e0-4703-9032-b95b836513b9
